import { Injectable } from '@angular/core';
import { PersianCalendarService } from './persian-calendar.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class UtilService {
    public base_url = 'http://';

    public token;
    constructor(
        private http: HttpClient,
        private persianCalendarService: PersianCalendarService,
    ) {
    }

    public get_today_fa() {
        return this.persianCalendarService.PersianCalendar(new Date());
    }
    public get_today_en() {
        return this.persianCalendarService.getGregoryFormat(new Date());
    }

    public add_persian_date(list) {
        for (let item of list) {
            item.p_date = this.persianCalendarService.PersianCalendar(new Date(item.create_at));
        }
        return list;
    }


    post(url_type, entity): Observable<any> {
        return this.http
            .post<any>(this.base_url + url_type, entity, httpOptions)
            .pipe(catchError(this.handleError<any>('post entity')));
    }

    get(url_type): Observable<any> {
        return this.http.get<any[]>(this.base_url + url_type)
            .pipe(catchError(this.handleError<any>(`get entities`)));
    }

    patch(url_type, entity: any, entity_id): Observable<any> {
        const url = `${this.base_url + url_type}/${entity_id}`;
        return this.http
            .patch<any>(url, entity, httpOptions)
            .pipe(catchError(this.handleError<any>('patch entity')));
    }

    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {
            // TODO: send the error to remote logging infrastructure
            console.error(error); // log to console instead
            // TODO: better job of transforming error for user consumption
            console.log(`${operation} failed: ${error.message}`);
            // Let the app keep running by returning an empty result.
            return of(result as T);
        };
    }

}
