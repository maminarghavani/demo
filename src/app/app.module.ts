import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';

// third party
import { ReCaptchaModule } from 'angular2-recaptcha';

// services
import { UtilService } from './providers/util.service';
import { PersianCalendarService } from './providers/persian-calendar.service';
import { AppRoutingModule } from './app-routing.module';

// components
import { LoginComponent } from './login/login';
import { RegisterComponent } from './register/register';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReCaptchaModule
  ],
  providers: [
    UtilService,
    PersianCalendarService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
