import {
    Component
} from '@angular/core';
import { Router } from '@angular/router';

// services
import { RegisterService } from './register.service';
import { UtilService } from '../providers/util.service';

@Component({
    selector: 'app-register',
    styleUrls: [
        './register.scss'
    ],
    templateUrl: './register.html',
    providers: [RegisterService]
})
export class RegisterComponent {

    public localState: any;
    constructor(
        private router: Router,
        private registerService: RegisterService,
        private utilService: UtilService,
    ) { }

    model = {
        username: '',
        password: '',
        password_repeat: ''
    };
    message = {
        text: '',
        type: ''
    };

    register() {
        if (this.model.password === this.model.password_repeat) {
            this.registerService.register(this.model)
                .subscribe(
                response => {
                    if (response.data === 'duplicate user') {
                        this.message = {
                            text: 'لطفا از نام کاربری دیگری استفاده کنید!',
                            type: 'danger'
                        };
                    } else {
                        localStorage.setItem('user', JSON.stringify(response.data.user));
                        this.router.navigate(['dashboard']);
                    }
                },
                error => {
                    this.message = {
                        text: 'ارتباط با سرور امکان پذیر نیست!',
                        type: 'danger'
                    };
                });
        } else {
            this.message = {
                text: 'مقادیر رمز ورود یکسان نیست!',
                type: 'danger'
            };
        }

    }
}
