import {
    Component,
    OnInit,
    ViewChild
} from '@angular/core';
import { Router } from '@angular/router';
import { ReCaptchaComponent } from 'angular2-recaptcha';

// services
import { LoginService } from './login.service';
import { UtilService } from '../providers/util.service';


@Component({
    selector: 'app-login',
    styleUrls: [
        './login.scss'
    ],
    templateUrl: './login.html',
    providers: [LoginService]
})
export class LoginComponent implements OnInit {
    @ViewChild(ReCaptchaComponent) captcha: ReCaptchaComponent;
    model = {
        username: '',
        password: ''
    };

    message = {
        text: '',
        type: ''
    };
    constructor(
        private router: Router,
        private loginService: LoginService,
        private utilService: UtilService,
    ) { }

    public ngOnInit() {
        const user = JSON.parse(localStorage.getItem('user'));
        if (user && this.utilService.token) {
            this.router.navigate(['dashboard']);
        }

    }

    public login() {
        if (this.captcha.getResponse()) {
            this.loginService.login(this.model)
                .subscribe(
                    response => {
                        if (response.status.code !== 200) {
                            this.message = {
                                text: 'نام کاربری و یا رمز عبور اشتباه است!',
                                type: 'danger'
                            };
                        } else {
                            localStorage.setItem('user', JSON.stringify(response.data.user));
                            this.utilService.token=response.data.token;
                            this.router.navigate(['dashboard']);
                        }
                    },
                    error => {
                        this.captcha.reset();
                        this.message = {
                            text: 'ارتباط با سرور امکان پذیر نیست!',
                            type: 'danger'
                        };
                    });
        } else {
            this.captcha.reset();
            this.message = {
                text: 'لطفا اطلاعات ریکپچا را کامل کنید!',
                type: 'danger'
            };
        }

    }

}
